use chrono::{NaiveDateTime, Datelike};

#[derive(Clone)]
pub struct Input {
    pub source: String,
    pub amount:f64,
    pub value: String
}

#[derive(Clone)]
pub struct Output {
    pub destination: String,
    pub amount:f64,
    pub value: String
}

impl Output {
    pub fn clone(&self) -> Self {
        Output {
            destination: self.destination.clone(),
            amount: self.amount,
            value: self.value.clone()
        }
    }
}

pub struct Transaction {
    pub date: NaiveDateTime,
    description: String,
    inputs: Vec<Input>,
    outputs:Vec<Output>,
    comments:Vec<String>
}

impl Transaction {
    pub fn new(date:NaiveDateTime, description:String) -> Transaction {
        Transaction {
            date,
            description,
            inputs:vec!(),
            outputs:vec!(),
            comments:vec!()
        }
    }

    pub fn add_input(&mut self, input: Input) {
        self.inputs.push(input);
    }

    pub fn add_output(&mut self, output: Output) {
        self.outputs.push(output);
    }

    pub fn add_comment(&mut self, comment: String) {
        self.comments.push(comment);
    }

    pub fn clone(&self) -> Self {
        Transaction {
            date: self.date,
            description: self.description.clone(),
            inputs: self.inputs.to_vec(),
            outputs: self.outputs.to_vec(),
            comments: self.comments.to_vec(),
        }
    }


    pub fn print(&self) {
        for comment in self.comments.iter() {
            println!(";{}", comment);
        }
        println!("{}/{}/{} {} * {}", self.date.date().year(),self.date.date().month(),self.date.date().day(), self.date.time(), self.description);
        for input in self.inputs.iter() {
            println!("\t {} \t\t -{} {}", input.source, input.amount, input.value);
        }
        for output in self.outputs.iter() {
            println!("\t {} \t\t {} {}", output.destination, output.amount, output.value);
        }
        println!("");
        println!("");
    }


}


