use crate::transaction::Transaction;

pub struct Account {
    pub transactions: Vec<Transaction>
}

impl Account {
    pub fn new() -> Account {
        Account {
            transactions: vec!{}
        }
    }

    pub fn add_transaction(&mut self, transaction: Transaction) {
        self.transactions.push(transaction);
    }

    pub fn print(&self) {
        for transaction in self.transactions.iter() {
           transaction.print();
        }
    }
}